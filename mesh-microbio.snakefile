configfile: 'config.yaml'


import datetime


OUTDIR=config['OUTDIR'] + '/' + config['MESH_DIR']
YEAR=str(datetime.date.today().year)


rule all:
    input:
        OUTDIR + '/microbio-mesh-terms.txt'


rule mesh_microbio:
    output:
        OUTDIR + '/microbio-mesh-terms.txt'

    input:
        script='scripts/roots2mesh-tree.py',
        mesh=OUTDIR + '/d' + YEAR + '.bin',
        roots='resources/microorganisms-roots.yaml'

    shell:
        '''{input.script} {input.mesh} {input.roots} >{output}'''
