configfile: 'config.yaml'


rule all:
    input:
        config['OUTDIR'] + '/compare.txt',


rule test_plans:
    output:
        config['OUTDIR'] + '/compare.txt'

    input:
        plan='alvisnlp/test.plan',
        root_paths=config['OUTDIR'] + '/microorganisms-root-paths.txt'

    shell:
        '''{config[ALVISNLP]} -param compare outFile {output} {input.plan} && tail -n 9 output/compare.txt | [ $(\grep -F -c 100.00) = "6" ]'''


rule microorganisms_root_paths:
    output:
        config['OUTDIR'] + '/microorganisms-root-paths.txt'

    input:
        'resources/microorganisms-roots.txt'

    shell:
        '''grep 'taxpath:' {input} | sed -e 's,^ [ ]*taxpath: ,,' -e 's,[ ]*$,/,' >{output}'''
