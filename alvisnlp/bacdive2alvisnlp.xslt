<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:a="xalan://fr.inra.maiage.bibliome.alvisnlp.bibliomefactory.modules.xml.XMLReader2"
                xmlns:inline="http://bibliome.jouy.inra.fr/alvisnlp/bibliome-module-factory/inline"
                extension-element-prefixes="a inline"
		>

  <xsl:param name="source-basename"/>

  <xsl:template match="/">
    <a:document xpath-id="substring-before($source-basename, '.xml')">
      <xsl:apply-templates select="root/taxonomy_name/strains/list-item"/>
      <xsl:apply-templates select="root/strain_availability/strains/list-item"/>
    </a:document>
  </xsl:template>

  <xsl:template match="taxonomy_name/strains/list-item">
    <a:section name="subspecies_epithet" xpath-contents="subspecies_epithet"/>
    <a:section name="species" xpath-contents="species"/>
    <a:section name="genus" xpath-contents="genus"/>
    <a:section name="family" xpath-contents="family"/>
    <a:section name="ordo" xpath-contents="ordo"/>
    <a:section name="class" xpath-contents="class"/>
    <a:section name="phylum" xpath-contents="phylum"/>
    <a:section name="domain" xpath-contents="domain"/>
    <a:section name="full_scientific_name" xpath-contents="normalize-space(full_scientific_name)"/>
    <a:section name="designation" xpath-contents="designation"/>
  </xsl:template>

  <xsl:template match="strain_availability/strains/list-item">
    <a:section name="strain_number" xpath-contents="strain_number"/>
  </xsl:template>
</xsl:stylesheet>