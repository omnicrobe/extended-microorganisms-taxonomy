configfile: 'config.yaml'


OUTDIR=config['OUTDIR'] + '/' + config['BACDIVE_STRAINS_DIR']


rule all:
    input:
        entry_list=OUTDIR + '/entry-ids.txt',
        entries=OUTDIR + '/entries',
        date=OUTDIR + '/DATE'


rule date:
    output:
        date=OUTDIR + '/DATE'

    shell:
         '''date '+%Y-%m-%d' >{output.date}'''


rule download_list:
    output:
        OUTDIR + '/all-entries.txt'

    shell:
        '''wget -O {output} 'https://bacdive.dsmz.de/advsearch/csv' '''


rule extract_entriy_ids:
    output:
        OUTDIR + '/entry-ids.txt'

    input:
        OUTDIR + '/all-entries.txt'

    shell:
        '''sed -e '1,3d' {input} | cut -d , -f 1 >{output}'''


# Approx. 3h
rule download_entries:
    output:
        directory(OUTDIR + '/entries')

    input:
        script='scripts/bacdive-download.py',
        passwd=config['BACDIVE_API_PASSWORD_FILE'],
        entry_list=OUTDIR + '/entry-ids.txt'

    params:
        user=config['BACDIVE_API_USER'],
        outdir=OUTDIR

    shell:
        '''{input.script} --output-directory {params.outdir} --bacdive-user {params.user} --bacdive-password-file {input.passwd} --entry-list-file {input.entry_list}'''
