#!/bin/env python3

import argparse
import sys
import yaml
from taxutils import Taxonomy, Logger, Saturator, Rejecter, NCBIParser


class OutputPatternMap:
    def __init__(self, pos_mapping):
        self.pos_mapping = pos_mapping
        self.taxon = None
        self.name = None

    def line(self, output_pattern, taxon, name):
        self.taxon = taxon
        self.name = name
        return output_pattern.format_map(self)

    def __getitem__(self, key):
        key = key.lower().replace('-', '').replace('_', '')
        if key == 'taxid':
            return self.taxon.taxid
        if key == 'rank':
            return self.taxon.rank.name
        if key == 'path' or key == 'taxidpath':
            return self.taxon.path
        if key == 'canonicalname' or key == 'canonical':
            canonical_name = self.taxon.canonical_name
            if canonical_name is None:
                raise RuntimeError('no canonical for %s' % self.taxon.taxid)
            return canonical_name.value
        if key == 'parent':
            if self.taxon.parent is None:
                return ''
            return self.taxon.parent.taxid
        if key.endswith('taxid') and len(key) > 5:
            rank = key[:-5]
            ancestor = self.taxon.ancestor(rank)
            if ancestor is None:
                return ''
            return ancestor.taxid
        if key.endswith('name') and len(key) > 4:
            rank = key[:-4]
            ancestor = self.taxon.ancestor(rank)
            if ancestor is None:
                return ''
            return ancestor.canonical_name.value
        if self.name is None:
            raise KeyError(key)
        if key == 'name' or key == 'synonym':
            return self.name.value
        if key == 'nametype':
            return self.name.name_type
        if key == 'pos' or key == 'postag':
            if self.name.name_type in self.pos_mapping:
                return self.pos_mapping[self.name.name_type]
            return 'NP'
        raise KeyError(key)


class RewriteTaxonomyApp(argparse.ArgumentParser):
    def __init__(self):
        argparse.ArgumentParser.__init__(self, description='Rewrite taxonomy')
        self.add_argument('--nodes', required=True, dest='nodes', action='append', default=[], help='nodes.dmp file')
        self.add_argument('--names', required=True, dest='names', action='append', default=[], help='names.dmp file')
        self.add_argument('--default-prefix', dest='default_prefix', action='store', default='ncbi', help='default taxid prefix')
        self.add_argument('--saturators', dest='saturators', action='store', help='saturator file (Yaml)')
        self.add_argument('--rejecters', dest='rejecters', action='store', help='rejecter file (Yaml)')
        self.add_argument('--reject-name-types', dest='reject_name_types', action='store', help='rejected name types file')
        self.add_argument('--iterate-names', dest='iterate_names', action='store_true', default=False, help='iterate through names')
        self.add_argument('--output-pattern', required=True, dest='output_pattern', action='store', help='output pattern')
        self.add_argument('--output-header', required=False, dest='output_header', default=None, action='store', help='output header')
        self.add_argument('--pos-mapping', dest='pos_mapping', action='store', help='POS mapping file (Yaml)')

    def get_reject_name_types(self, args):
        if args.reject_name_types:
            Logger.info('reading name types reject: %s' % args.reject_name_types)
            with open(args.reject_name_types) as f:
                return set(line.strip() for line in f)
        return set()

    def get_saturators(self, args):
        if args.saturators:
            return Saturator.from_yaml_file(args.saturators)
        return Saturator()

    def get_rejecters(self, args):
        if args.rejecters:
            return Rejecter.from_yaml_file(args.rejecters)
        return Rejecter()

    def get_pos_mapping(self, args):
        if args.pos_mapping:
            Logger.info('reading POS mapping file: %s' % args.pos_mapping)
            with open(args.pos_mapping) as f:
                return yaml.load(f, Loader=yaml.FullLoader)
        return {}

    @staticmethod
    def _output_escape(s):
        return s.replace('\\n', '\n').replace('\\t', '\t').replace('\\r', '\r')

    def run(self):
        args = self.parse_args()
        taxonomy = Taxonomy()
        for node in args.nodes:
            with open(node) as f:
                NCBIParser.read_node_file(f, taxonomy)
        taxonomy.resolve_parents()
        reject_name_types = self.get_reject_name_types(args)
        for name in args.names:
            with open(name) as f:
                NCBIParser.read_name_file(f, taxonomy, reject_name_types)
        if args.output_header is not None:
            sys.stdout.write(RewriteTaxonomyApp._output_escape(args.output_header))
        output_pattern = RewriteTaxonomyApp._output_escape(args.output_pattern)
        if args.iterate_names:
            saturators = self.get_saturators(args)
            rejecters = self.get_rejecters(args)
            pos_mapping = self.get_pos_mapping(args)
            output_pattern_map = OutputPatternMap(pos_mapping)
            Logger.info('writing')
            for taxon in taxonomy.taxa():
                for name in saturators.saturate_reject(rejecters, taxon):
                    sys.stdout.write(output_pattern_map.line(output_pattern, taxon, name))
        else:
            output_pattern_map = OutputPatternMap({})
            Logger.info('writing')
            for taxon in taxonomy.taxa():
                sys.stdout.write(output_pattern_map.line(output_pattern, taxon, taxon.canonical_name))
        Logger.info('done')


if __name__ == '__main__':
    app = RewriteTaxonomyApp()
    app.run()
