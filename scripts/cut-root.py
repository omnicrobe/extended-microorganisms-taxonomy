#!/bin/env python3


import sys
import re
import yaml
import taxutils


TAXO_FILE = sys.argv[1]
with (open(TAXO_FILE) as f):
    TAXO = taxutils.NCBIParser.read_node_file(f)
TAXO.resolve_parents()


ROOT_FILE = sys.argv[2]
ROOT_CANDIDATES: list
with open(ROOT_FILE) as f:
    ROOT_CANDIDATES = list((e['name'], TAXO.get_taxon(e['taxid'])) for e in yaml.load(f, Loader=yaml.FullLoader) if 'taxid' in e)


ROOTS = []
for name1, taxon1 in ROOT_CANDIDATES:
    accept = True
    for name2, taxon2 in ROOT_CANDIDATES:
        if taxon1 == taxon2 and name1 == name2:
            continue
        if taxon1 in taxon2.ancestors():
            sys.stderr.write('%s excluded since it is subsumed by %s\n' % (name1, name2))
            accept = False
    if accept:
        sys.stderr.write('Root %s (%s)\n' % (name1, taxon1.path))
        ROOTS.append((name1, taxon1.path))


PATTERN = re.compile(r'\t(?:' + '|'.join(path for (name, path) in ROOTS) + r')[/\t]')
REMAIN = list(ROOTS)
for line in sys.stdin:
    m = PATTERN.search(line)
    if m is not None:
        sys.stdout.write(line)
        for name, path in REMAIN:
            if path in line:
                REMAIN.remove((name, path))
for name, path in REMAIN:
    sys.stderr.write('%s not seen\n' % (name,))
