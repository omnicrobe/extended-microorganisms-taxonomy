#!/bin/env python


import logging
import subprocess
import json
import os
import os.path
import stat
import argparse

BACDIVE_AUTH_URL = 'https://sso.dsmz.de/auth/realms/dsmz/protocol/openid-connect/token'
BACDIVE_API_URL = 'https://api.bacdive.dsmz.de'
CHUNK_SIZE = 100


logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
Logger = logging.getLogger(__name__)


class BacdiveDownloadApp(argparse.ArgumentParser):
    def __init__(self):
        argparse.ArgumentParser.__init__(self, description='Download BacDive entries')
        self.add_argument('--output-directory', required=True, dest='outdir', action='store', help='directory where to store entry files')
        self.add_argument('--bacdive-user', required=True, dest='bacdive_user', action='store', help='username for BacDive API services')
        self.add_argument('--bacdive-password-file', required=True, dest='bacdive_password_file', help='file containing the password for BacDive API services (chmod 600)')
        self.add_argument('--entry-list-file', required=True, dest='entry_list_file', help='file containing the list of BacDive entry identifiers to download')

    def authenticate(self):
        with open(self.args.bacdive_password_file) as pf:
            password = pf.read().strip()
        Logger.info('authenticating to BacDive')
        self.run_auth('authentication', 'password', username=self.args.bacdive_user, password=password)

    def refresh(self):
        Logger.info('refreshing token')
        self.run_auth('refresh_token', 'refresh_token', refresh_token=self.refresh_token)

    def run_auth(self, step_name, grant_type, **data):
        cl = self.curl_auth_commandline(grant_type, **data)
        result = subprocess.run(cl, capture_output=True, text=True)
        if result.returncode != 0:
            raise RuntimeError('error during ' + step_name)
        jcontent = json.loads(result.stdout)
        if 'error' in jcontent:
            raise RuntimeError(step_name + ' failed: ' + jcontent['error_description'])
        self.retrieve_auth_tokens(jcontent)

    def curl_auth_commandline(self, grant_type, **data):
        cl = ['curl', '-s', '-S', '-L', '-X', 'POST', BACDIVE_AUTH_URL, '-H', 'Content-Type: application/x-www-form-urlencoded', '--data-urlencode', 'client_id=api.bacdive.public', '--data-urlencode', 'grant_type=' + grant_type]
        for k, v in data.items():
            cl.append('--data-urlencode')
            cl.append(k + '=' + v)
        return cl

    def retrieve_auth_tokens(self, jcontent):
        access_token_file = os.path.join(self.args.outdir, 'access-token.txt')
        refresh_token_file = os.path.join(self.args.outdir, 'refresh-token.txt')
        self.access_token = jcontent['access_token']
        self.write_token(access_token_file, self.access_token)
        self.refresh_token = jcontent['refresh_token']
        self.write_token(refresh_token_file, self.refresh_token)

    def write_token(self, filename, token):
        with open(filename, 'w') as f:
            f.write(token)
        os.chmod(filename, stat.S_IRUSR | stat.S_IWUSR)

    def fetch_entries(self, ids):
        os.makedirs(os.path.join(self.args.outdir, 'entries'), exist_ok=True)
        filename = os.path.join(self.args.outdir, 'entries', '%06d-%06d.json' % (ids[0], ids[-1]))
        if os.path.exists(filename):
            Logger.info('file %s exists, skipping...' % filename)
            return
        cl = ['curl', '-s', '-S', '-L', '-X', 'GET', BACDIVE_API_URL + '/fetch/' + ';'.join(str(i) for i in ids), '-H', 'Content-Type: application/x-www-form-urlencoded', '-H', 'Authorization: Bearer ' + self.access_token, '-H', 'Connection: Keep-Alive', '-H', 'Keep-Alive: 180']
        Logger.info('fetching from %d to %d' % (ids[0], ids[-1]))
        result = subprocess.run(cl, capture_output=True, text=True)
        if result.returncode != 0:
            raise RuntimeError('error during fetch: ' + result.stderr)
        jcontent = json.loads(result.stdout)
        if 'error' in jcontent:
            raise RuntimeError('fetch failed: ' + jcontent['error_description'])
        if 'code' in jcontent and jcontent['code'] == 401 and jcontent['message'] == 'Expired token':
            self.refresh()
            self.fetch_entries(ids)
        else:
            with open(filename, 'w') as f:
                json.dump(jcontent, f)

    def fetch_all_entries(self):
        all_ids = self.get_all_entry_ids()
        all_ids.sort()
        Logger.info('Found %d entries, highest id: %d' % (len(all_ids), all_ids[-1]))
        for cs in range(0, len(all_ids), CHUNK_SIZE):
            ce = cs + CHUNK_SIZE
            chunk = all_ids[cs:ce]
            self.fetch_entries(chunk)

    def get_all_entry_ids(self):
        Logger.info('retrieving all entry ids')
        with open(self.args.entry_list_file) as f:
            return list(int(line) for line in f)

    def run(self):
        self.args = self.parse_args()
        self.authenticate()
        self.fetch_all_entries()
        Logger.info('done')


if __name__ == '__main__':
    BacdiveDownloadApp().run()
