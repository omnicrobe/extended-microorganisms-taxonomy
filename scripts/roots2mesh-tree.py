#!/bin/env python3


import sys
import yaml
import mesh


MESH_FILE = sys.argv[1]
ROOTS_FILE = sys.argv[2]


with open(ROOTS_FILE) as f:
    ROOTS = dict((e['mesh-id'], e['name']) for e in yaml.load(f, Loader=yaml.FullLoader) if 'mesh-id' in e)


SEEN = set()


for rec in mesh.records(MESH_FILE):
    mid = rec.uid
    if mid not in ROOTS:
        continue
    if not rec.MN:
        raise RuntimeError('no tree for ' + mid)
    for mtree in rec.MN:
        print(mtree)
    SEEN.add(mid)


for meshid, name in ROOTS.items():
    if meshid not in SEEN:
        sys.stderr.write('not seen: %s %s' % (meshid, name))
