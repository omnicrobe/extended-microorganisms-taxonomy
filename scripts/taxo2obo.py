#!/bin/env python3


import argparse
from taxutils import Logger, Taxonomy, NCBIParser
import obo
import sys


class Taxo2OBOApp(argparse.ArgumentParser):
    def __init__(self):
        argparse.ArgumentParser.__init__(self, description='Rewrite taxonomy')
        self.add_argument('--nodes', required=True, dest='nodes', action='append', default=[], help='nodes.dmp file')
        self.add_argument('--names', required=True, dest='names', action='append', default=[], help='names.dmp file')
        self.add_argument('--include-synonyms', required=False, dest='include_synonyms', action='store_true', default=False, help='include synonyms')
        self.add_argument('--standard-ranks', required=False, dest='standard_ranks', action='store_true', default=False, help='only include standard ranks')
        self.add_argument('--subspecific-ranks', required=False, dest='subspecific', action='store_true', default=False, help='only include subspecific ranks')
        self.add_argument('--root', required=False, dest='rootids', action='append', default=[], help='only include subtree')
        self.add_argument('--roots-file', required=False, dest='roots_file', action='store', default=None, help='only include subtree from file')

    def _accept_rank(self, taxon):
        if taxon.taxid in self.args.rootids:
            return True
        if (not self.args.standard_ranks) and (not self.args.subspecific):
            return True
        if self.args.standard_ranks and taxon.rank.is_standard:
            return True
        if self.args.subspecific and taxon.is_subspecific:
            return True
        return False

    def _get_parent(self, taxon):
        if taxon.taxid in self.args.rootids:
            return None
        parent = taxon.parent
        while parent is not None:
            if self._accept_rank(parent):
                return parent
            parent = parent.parent
        return None

    def _itertaxa(self, taxonomy):
        if len(self.args.rootids) == 0:
            yield from taxonomy.taxa()
        else:
            for rootid in self.args.rootids:
                root = taxonomy[rootid]
                yield from root.descendants(include_self=True)

    def run(self):
        self.args = self.parse_args()
        taxonomy = Taxonomy()
        for node in self.args.nodes:
            with open(node) as f:
                NCBIParser.read_node_file(f, taxonomy)
        taxonomy.resolve_parents()
        if self.args.roots_file is not None:
            with open(self.args.roots_file) as f:
                self.args.rootids.extend(rootid.strip() for rootid in f)
        roots = list(taxonomy[rootid] for rootid in self.args.rootids)
        included = []
        for r1 in roots:
            for r2 in roots:
                if r1 in r2.ancestors():
                    Logger.info('%s contains %s' % (r1.taxid, r2.taxid))
                    included.append(r2.taxid)
        if included:
            Logger.warning('ignoring included roots')
            self.args.rootids = list(rootid for rootid in self.args.rootids if rootid not in included)
        for name in self.args.names:
            with open(name) as f:
                NCBIParser.read_name_file(f, taxonomy)
        Logger.info('converting')
        onto = obo.Ontology()
        for taxon in self._itertaxa(taxonomy):
            if not self._accept_rank(taxon):
                continue
            term = obo.Term(taxon.source, taxon.lineno, onto, obo.SourcedValue(taxon.source, taxon.lineno, taxon.taxid))
            parent = self._get_parent(taxon)
            if parent is not None:
                obo.StanzaReference(taxon.source, taxon.lineno, term, 'is_a', parent.taxid)
            name = taxon.canonical_name
            term.name = obo.SourcedValue(name.source, name.lineno, name.value)
            if self.args.include_synonyms:
                for name in taxon.names():
                    if not name.canonical:
                        obo.Synonym(name.source, name.lineno, term, name.value, 'EXACT', None, None)
        onto.resolve_references(obo.DanglingReferenceFail(), obo.DanglingReferenceWarn())
        Logger.info('writing')
        onto.write_obo(sys.stdout)
        for term in onto.iterterms():
            term.write_obo(sys.stdout)
        Logger.info('done')


if __name__ == '__main__':
    app = Taxo2OBOApp()
    app.run()
