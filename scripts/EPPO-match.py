import taxutils
import collections
import os
import os.path


OUTDIR = os.path.join('output', 'EPPO-match')
os.makedirs(OUTDIR, exist_ok=True)


def read_eppo():
    eppo_taxonomy = taxutils.EPPOParser.read_xmlfull_file('output/EPPO/fullcodes.xml')
    eppo_taxonomy.resolve_parents()
    return eppo_taxonomy


def read_ncbi():
    with open('output/ncbi-taxonomy/nodes.dmp') as f:
        ncbi_taxonomy = taxutils.NCBIParser.read_node_file(f)
    ncbi_taxonomy.resolve_parents()
    with open('output/ncbi-taxonomy/names.dmp') as f:
        taxutils.NCBIParser.read_name_file(f, ncbi_taxonomy)
    return ncbi_taxonomy


def index_ncbi_names(ncbi_taxonomy):
    taxutils.Logger.info('indexing NCBI names')
    ncbi_name_index = collections.defaultdict(set)
    for taxon in ncbi_taxonomy.taxa():
        for name in taxon.names(lambda n: n.name_type != 'common name' and n.name_type != 'in-part'):
            ncbi_name_index[name.value].add(taxon)
    return ncbi_name_index


def search_eppo_names(ncbi_taxonomy, eppo_taxonomy, ncbi_name_index):
    taxutils.Logger.info('searching EPPO names')
    mapping = taxutils.TaxonomyMapping(ncbi_taxonomy, eppo_taxonomy)
    for eppo_taxon in eppo_taxonomy.taxa():
        matched_ncbi = set()
        for eppo_name in eppo_taxon.names(lambda n: n.name_type.startswith('scientific')):
            for ncbi_taxon in ncbi_name_index[eppo_name.value]:
                matched_ncbi.add(ncbi_taxon)
        for ncbi_taxon in matched_ncbi:
            al = mapping.add_alignment(ncbi_taxon, eppo_taxon)
            al.info['common-names'] = True
            al.info['same-rank'] = (ncbi_taxon.rank == eppo_taxon.rank)
    return mapping


def matched_parents(mapping, suffix='', pred=(lambda _: True)):
    taxutils.Logger.info('looking for matched parents')
    eppo_taxonomy = mapping.taxonomy2
    for eppo_taxon in eppo_taxonomy.taxa():
        als = list(al for al in mapping.get_21(eppo_taxon) if pred)
        for al in als:
            al.info['unambiguous' + suffix] = len(als) < 2
        eppo_parent = eppo_taxon.parent
        if eppo_parent is None:
            ncbi_aligned_parents = set()
        else:
            ncbi_aligned_parents = set(al.taxon1 for al in mapping.get_21(eppo_parent) if pred)
        aligned_homonym_parents = set()
        for al in als:
            aligned_ancestors = ncbi_aligned_parents & set(al.taxon1.ancestors())
            al.info['aligned-parent' + suffix] = aligned_ancestors
            aligned_homonym_parents |= aligned_ancestors
        for al in als:
            al.info['homonym-with-aligned-parent' + suffix] = aligned_ancestors


eppo_taxonomy = read_eppo()
ncbi_taxonomy = read_ncbi()
ncbi_name_index = index_ncbi_names(ncbi_taxonomy)
mapping = search_eppo_names(ncbi_taxonomy, eppo_taxonomy, ncbi_name_index)
matched_parents(mapping)
matched_parents(mapping, suffix='-2', pred=(lambda al: al.info['unambiguous'] or al.info['aligned-parent']))


def outfile(filename):
    fullname = os.path.join(OUTDIR, filename)
    taxutils.Logger.info('writing %s' % fullname)
    return open(fullname, 'w')


with outfile('matched-codes.csv') as f:
    f.write('\t'.join((
        'EPPO code',
        'EPPO name',
        'EPPO rank',
        'NCBI code',
        'NCBI name',
        'NCBI rank',
        'same rank',
        'unambiguous',
        'aligned parent',
        'homonym with aligned parent',
        '# common names',
        'common names'
    )))
    f.write('\n')
    for eppo_taxon in eppo_taxonomy.taxa():
        if mapping.has_21(eppo_taxon):
            eppo_taxon_names = set(n.value for n in eppo_taxon.names())
            for alignment in mapping.get_21(eppo_taxon):
                ncbi_taxon = alignment.taxon1
                ncbi_taxon_names = set(n.value for n in ncbi_taxon.names())
                names_in_common = ncbi_taxon_names & eppo_taxon_names
                f.write('\t'.join((
                    eppo_taxon.taxid,
                    eppo_taxon.canonical_name.value,
                    eppo_taxon.rank.name,
                    ncbi_taxon.taxid,
                    ncbi_taxon.canonical_name.value,
                    ncbi_taxon.rank.name,
                    str(alignment.info['same-rank']),
                    str(alignment.info['unambiguous']),
                    ', '.join(('%s (%s)' % (t.taxid, t.canonical_name.value)) for t in alignment.info['aligned-parent']),
                    ', '.join(('%s (%s)' % (t.taxid, t.canonical_name.value)) for t in alignment.info['homonym-with-aligned-parent']),
                    str(len(names_in_common)),
                    ', '.join(names_in_common)
                )))
                f.write('\n')


with outfile('unmatched-codes.csv') as f:
    f.write('\t'.join((
        'EPPO code',
        'EPPO name',
        'EPPO rank',
    )))
    f.write('\n')
    for eppo_taxon in eppo_taxonomy.taxa():
        if not mapping.has_21(eppo_taxon):
            f.write('\t'.join((
                eppo_taxon.taxid,
                eppo_taxon.canonical_name.value,
                eppo_taxon.rank.name
            )))
            f.write('\n')


CLIQUES = {}
CLIQUE_MAP = {}
for next_clique, eppo_taxon in enumerate(eppo_taxonomy.taxa()):
    if eppo_taxon.taxid in CLIQUE_MAP:
        eppo_clique = CLIQUE_MAP[eppo_taxon.taxid]
        eppo_clique_ncbi_taxa, eppo_clique_eppo_taxa = CLIQUES[eppo_clique]
    else:
        eppo_clique = next_clique
        eppo_clique_ncbi_taxa = []
        eppo_clique_eppo_taxa = [eppo_taxon]
        CLIQUES[eppo_clique] = (eppo_clique_ncbi_taxa, eppo_clique_eppo_taxa)
        CLIQUE_MAP[eppo_taxon.taxid] = eppo_clique
    if mapping.has_21(eppo_taxon):
        for alignment in mapping.get_21(eppo_taxon):
            ncbi_taxon = alignment.taxon1
            if ncbi_taxon.taxid in CLIQUE_MAP:
                ncbi_clique = CLIQUE_MAP[ncbi_taxon.taxid]
                if ncbi_clique == eppo_clique:
                    continue
                (ncbi_clique_ncbi_taxa, ncbi_clique_eppo_taxa) = CLIQUES[ncbi_clique]
                eppo_clique_ncbi_taxa.extend(ncbi_clique_ncbi_taxa)
                eppo_clique_eppo_taxa.extend(ncbi_clique_eppo_taxa)
                for t in ncbi_clique_ncbi_taxa:
                    CLIQUE_MAP[t.taxid] = eppo_clique
                for t in ncbi_clique_eppo_taxa:
                    CLIQUE_MAP[t.taxid] = eppo_clique
                del CLIQUES[ncbi_clique]
            else:
                CLIQUE_MAP[ncbi_taxon.taxid] = eppo_clique
                eppo_clique_ncbi_taxa.append(ncbi_taxon)


with outfile('cliques.csv') as f:
    eppo_max = 0
    ncbi_max = 0
    both_max = 0
    for ncbi_taxa, eppo_taxa in CLIQUES.values():
        eppo_max = max(eppo_max, len(eppo_taxa))
        ncbi_max = max(ncbi_max, len(ncbi_taxa))
        both_max = max(both_max, len(eppo_taxa) + len(ncbi_taxa))
        if len(eppo_taxa) == 1:
            if len(ncbi_taxa) < 2:
                continue
            prefix = '>'
        elif len(ncbi_taxa) == 1:
            prefix = '<'
        else:
            prefix = '*'
        f.write('%s %s = %s\n' % (prefix, ', '.join(t.taxid for t in eppo_taxa), ', '.join(t.taxid for t in ncbi_taxa)))
    f.write('eppo_max = %d\n' % eppo_max)
    f.write('ncbi_max = %d\n' % ncbi_max)
    f.write('both_max = %d\n' % both_max)


def index_eppo_names(eppo_taxonomy):
    taxutils.Logger.info('indexing EPPO names')
    eppo_name_index = collections.defaultdict(set)
    for taxon in eppo_taxonomy.taxa():
        for name in taxon.names(lambda n: n.name_type.startswith('scientific')):
            eppo_name_index[name.value].add(taxon)
    return eppo_name_index


def collect_eppo_ambiguous(eppo_taxonomy, eppo_name_index):
    taxutils.Logger.info('gathering ambiguous')
    eppo_self_mapping = taxutils.TaxonomyMapping(eppo_taxonomy, eppo_taxonomy)
    for taxa in eppo_name_index.values():
        taxalist = list(taxa)
        while len(taxalist) > 0:
            taxon1 = taxalist.pop()
            for taxon2 in taxalist:
                eppo_self_mapping.extend_alignment(taxon1, taxon2)
    return eppo_self_mapping


eppo_name_index = index_eppo_names(eppo_taxonomy)
eppo_self_mapping = collect_eppo_ambiguous(eppo_taxonomy, eppo_name_index)


with outfile('EPPO-ambiguous.csv') as f:
    for taxon1 in eppo_taxonomy.taxa():
        ns1 = set(n.value for n in taxon1.names())
        for al in eppo_self_mapping.get_12(taxon1):
            ns2 = set(n.value for n in al.taxon2.names())
            f.write('%s\t%s\t%s\n' % (taxon1.taxid, al.taxon2.taxid, ', '.join(ns1 & ns2)))


taxutils.Logger.info('Done')
