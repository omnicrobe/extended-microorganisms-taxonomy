#!/bin/env python3

from taxutils import Logger, Rank, Taxon, NCBIParser, BacDiveParser, TaxonomyMapping
import re
import collections
import argparse


class BacdiveOnNCBIMapping(TaxonomyMapping):
    STRAIN_RANKS = set((Rank.norank, Rank.strain, Rank.isolate, Rank.serotype, Rank.biotype))

    def __init__(self, ncbi_taxonomy, bacdive_taxonomy):
        TaxonomyMapping.__init__(self, ncbi_taxonomy, bacdive_taxonomy)
        Logger.info('creating NCBI name index')
        self.ncbi_names = ncbi_taxonomy.get_name_map()
        self.disqualifying_adjectives = ()
        self.legitimate_collections = ()
        self.priority_collections = ()

    @property
    def ncbi_taxonomy(self):
        return self.taxonomy1

    @property
    def bacdive_taxonomy(self):
        return self.taxonomy2

    def read_disqualifying_adjectives(self, f):
        self.disqualifying_adjectives = tuple(re.compile('\\b' + line.strip() + '\\b') for line in f if line.strip() != '')

    def read_legitimate_collections(self, f):
        self.legitimate_collections = tuple(line.strip() for line in f if line.strip() != '')

    def read_priority_collections(self, f):
        self.priority_collections = tuple(line.strip() for line in f if line.strip() != '')

    def align_higher_ranks(self):
        Logger.info('matching higher rank taxa')
        total = 0
        unmatched = 0
        reranked = collections.defaultdict(int)
        for bacdive_taxon in self.bacdive_taxonomy.top_down_taxa(Taxon.has_not_rank(Rank.strain)):
            total += 1
            canonical_name = bacdive_taxon.canonical_name.value
            if canonical_name in self.ncbi_names:
                ncbi_names = self.ncbi_names[canonical_name]
                ncbi_taxa = set(n.taxon for n in ncbi_names)
                ncbi_taxa_best = self._find_best_ncbi_taxa(bacdive_taxon, ncbi_taxa)
                if len(ncbi_taxa_best) == 0:
                    raise RuntimeError()
                if len(ncbi_taxa_best) > 1:
                    Logger.warning('%s (%s) has several matches: %s' % (canonical_name, bacdive_taxon.rank.name, ', '.join(t.taxid for t in ncbi_taxa_best)))
                ncbi_taxon = next(iter(ncbi_taxa_best))
                al = self.add_alignment(ncbi_taxon, bacdive_taxon)
                al.info['type'] = 'equivalent'
                if ncbi_taxon.rank != bacdive_taxon.rank:
                    reranked[(ncbi_taxon.rank.name, bacdive_taxon.rank.name)] += 1
            else:
                # Logger.warning('no match for higher rank %s' % canonical_name)
                unmatched += 1
        Logger.info('%d higher rank taxa' % total)
        Logger.info('  %d not matched' % unmatched)
        for (brank, nrank), n in reranked.items():
            Logger.info('  %s reranked from %s to %s' % (n, brank, nrank))

    def _find_best_ncbi_taxa(self, bacdive_taxon, ncbi_taxa):
        ncbi_taxa_at_rank = set(filter(Taxon.has_rank(bacdive_taxon.rank), ncbi_taxa))
        ncbi_taxa_with_equivalent_ancestor = set(t for t in ncbi_taxa if self._has_equivalent_ancestor(t))
        ncbi_taxa_both = ncbi_taxa_at_rank & ncbi_taxa_with_equivalent_ancestor
        if len(ncbi_taxa_both) > 0:
            return ncbi_taxa_both
        if len(ncbi_taxa_at_rank) > 0:
            return ncbi_taxa_at_rank
        if len(ncbi_taxa_with_equivalent_ancestor) > 0:
            return ncbi_taxa_with_equivalent_ancestor
        return ncbi_taxa

    def _has_equivalent_ancestor(self, ncbi_taxon):
        for ancestor in ncbi_taxon.ancestors():
            if self.has_12(ancestor, lambda a: a.info['type'] == 'equivalent'):
                return True
        return False

    def align_strains(self):
        Logger.info('matching strains')
        noutcomes = collections.defaultdict(int)
        nalign = collections.defaultdict(int)
        total = 0
        for taxon in self.bacdive_taxonomy.taxa(Taxon.has_rank(Rank.strain)):
            matched_taxon, alignment_type, outcome = self._match_strain(taxon)
            if matched_taxon is not None:
                if alignment_type == 'split':
                    taxon_names = set(taxon.name_values())
                    matched_taxon_names = set(matched_taxon.name_values())
                    if len(matched_taxon_names - taxon_names) == 0:
                        alignment_type = 'equivalent'
                        outcome = 'equivalent by name depletion'
                al = self.add_alignment(matched_taxon, taxon)
                al.info['type'] = alignment_type
                nalign[alignment_type] += 1
            else:
                Logger.warning('no match for %s (%s)' % (taxon.taxid, next(iter(taxon.names())).value))
            total += 1
            noutcomes[outcome] += 1
        Logger.info('%d strains' % total)
        for outcome, n in sorted(noutcomes.items()):
            Logger.info('  %s: %d' % (outcome, n))
        for at, n in nalign.items():
            Logger.info('  -> %s: %d' % (at, n))

    def _match_strain(self, taxon):
        matched_taxa = set(self._get_matched_taxa(taxon))
        if len(matched_taxa) == 0:
            return self._try_strain_ancestors(taxon)
        matched_taxon, outcome = self._select_matched_taxon(taxon, matched_taxa)
        if matched_taxon is None:
            return None, None, outcome
        if matched_taxon.rank in BacdiveOnNCBIMapping.STRAIN_RANKS:
            return matched_taxon, 'equivalent', outcome
        return matched_taxon, 'split', outcome

    def _get_matched_taxa(self, taxon):
        for name in taxon.names():
            if name.value.isdigit():
                continue
            if name.value in self.ncbi_names:
                for n in self.ncbi_names[name.value]:
                    yield n.taxon

    def _try_strain_ancestors(self, taxon):
        for ancestor in taxon.ancestors():
            for al in self.get_21(ancestor, lambda a: a.info['type'] == 'equivalent'):
                other = al.taxon1
                if other.rank == Rank.species or other.rank == Rank.subspecies:
                    return other, 'append', 'append to [sub]species ancestor'
                return other, 'append', 'append to ancestor of high rank'
        return None, None, 'no match'

    def _has_no_child(self, taxon, matched_taxa):
        for t in matched_taxa:
            for a in t.ancestors():
                if a == taxon:
                    return False
        return True

    def _select_matched_taxon(self, taxon, matched_taxa):
        if len(matched_taxa) == 1:
            return next(iter(matched_taxa)), 'no ambiguity'
        without_ancestors = list(t for t in matched_taxa if self._has_no_child(t, matched_taxa))
        if len(matched_taxa) == 1:
            return without_ancestors[0], 'disambiguate-1 by child'
        taxon_names = set(taxon.name_values())
        matched_names = list((t, set(taxon_names & set(t.name_values()))) for t in without_ancestors)
        highest_number = max(len(ns) for (t, ns) in matched_names)
        highest_number_matched_taxa = list((t, ns) for (t, ns) in matched_names if len(ns) == highest_number)
        if len(highest_number_matched_taxa) == 1:
            return highest_number_matched_taxa[0][0], 'disambiguate-2 by number of matched names'
        qualified = list((t, ns) for (t, ns) in highest_number_matched_taxa if self._is_qualified(t))
        if len(qualified) == 1:
            return qualified[0][0], 'disambiguate-3 by ancestor name disqualification'
        if len(qualified) == 0:
            qualified = highest_number_matched_taxa
        legitimate = list((t, ns) for (t, ns) in qualified if self._find_collections(ns, self.legitimate_collections))
        if len(legitimate) == 1:
            return legitimate[0][0], 'disambiguate-4 by legitimate collection'
        priority = list((t, ns) for (t, ns) in legitimate if self._find_collections(ns, self.priority_collections))
        if len(priority) == 1:
            return priority[0][0], 'disambiguate-5 by priority collection'
        Logger.warning('for %s (%s), could not disambiguate between:' % (taxon.taxid, next(iter(taxon.names())).value))
        for other, ns in qualified:
            Logger.warning('  %s (%s) -> %s' % (other.taxid, other.canonical_name.value, ', '.join(ns)))
        return None, 'ambiguous'

    def _is_qualified(self, taxon):
        for ancestor in taxon.ancestors(include_self=True):
            for disqualifying_adjective in self.disqualifying_adjectives:
                m = disqualifying_adjective.search(ancestor.canonical_name.value)
                if m is not None:
                    return False
        return True

    def _find_collections(self, names, colls):
        for n in names:
            for c in colls:
                if c in n:
                    return True
        return False


class BacdiveMatchApp(argparse.ArgumentParser):
    def __init__(self):
        argparse.ArgumentParser.__init__(self, description='Rewrite taxonomy')
        self.add_argument('--nodes', required=True, dest='nodes', action='store', default=None, help='nodes.dmp file')
        self.add_argument('--names', required=True, dest='names', action='store', default=None, help='names.dmp file')
        self.add_argument('--bacdive', required=True, dest='bacdive', action='store', default=None, help='path to directory containing BacDive XML files')
        self.add_argument('--disqualifying-adjectives', required=True, dest='disqualifying_adj', action='store', default=None, help='path to disqualifying adjectives file')
        self.add_argument('--legitimate-collections', required=True, dest='legit_coll', action='store', default=None, help='path to legitimate collections file')
        self.add_argument('--priority-collections', required=True, dest='priority_coll', action='store', default=None, help='path to priority collections file')
        self.add_argument('--output', required=True, dest='output', action='store', default=None, help='path to directory where to write files')

    def run(self):
        args = self.parse_args()
        self.output_dir = args.output
        self.ncbi_taxonomy = self._read_ncbi_taxonomy(args.nodes, args.names)
        self.bacdive_taxonomy = self._read_bacdive_taxonomy(args.bacdive)
        self.mapping = BacdiveOnNCBIMapping(self.ncbi_taxonomy, self.bacdive_taxonomy)
        with open(args.disqualifying_adj) as f:
            self.mapping.read_disqualifying_adjectives(f)
        with open(args.legit_coll) as f:
            self.mapping.read_legitimate_collections(f)
        with open(args.priority_coll) as f:
            self.mapping.read_priority_collections(f)
        self.mapping.align_higher_ranks()
        self.mapping.align_strains()
        self._write_output()
        Logger.info('done')

    def _read_ncbi_taxonomy(self, node_file, name_file):
        with open(node_file) as f:
            taxonomy = NCBIParser.read_node_file(f)
        taxonomy.resolve_parents()
        with open(name_file) as f:
            NCBIParser.read_name_file(f, taxonomy)
        return taxonomy

    def _read_bacdive_taxonomy(self, path):
        return BacDiveParser.read_directory(path)

    def _oof(self, fn):
        ffn = self.output_dir + '/' + fn
        Logger.info('writing ' + ffn)
        return open(ffn, 'w')

    def _write_output(self):
        self._write_dispatch()
        self._write_mapping()
        self._write_bacdive_nodes()
        self._write_bacdive_names()
        self._write_ncbi_names()

    def _write_dispatch(self):
        with self._oof('dispatch-report.txt') as f:
            for taxon in self.bacdive_taxonomy.taxa(Taxon.has_rank(Rank.strain)):
                if self.mapping.has_21(taxon):
                    al = next(iter(self.mapping.get_21(taxon)))
                    f.write('%s\t%s\t%s\n' % (taxon.taxid, al.info['type'], al.taxon1.taxid))
                else:
                    f.write('%s\tnone\t\n' % taxon.taxid)

    def _write_mapping(self):
        with self._oof('bacdive-to-taxid.txt') as f:
            for taxon in self.bacdive_taxonomy.taxa(Taxon.has_rank(Rank.strain)):
                bid = taxon.taxid.replace('bd:', '')
                for al in self.mapping.get_21(taxon):
                    if al.info['type'] == 'equivalent':
                        tid = al.taxon1.taxid
                    else:
                        tid = taxon.taxid
                    f.write('%s\t%s\n' % (bid, tid))

    def _write_bacdive_nodes(self):
        with self._oof('bacdive-nodes.dmp') as f:
            for taxon in self.bacdive_taxonomy.taxa(Taxon.has_rank(Rank.strain)):
                for al in self.mapping.get_21(taxon):
                    other = al.taxon1
                    at = al.info['type']
                    if at == 'append' or at == 'split':
                        f.write('\t|\t'.join((
                            taxon.taxid,  # taxid
                            other.taxid,  # parent taxid
                            taxon.rank.name,  # rank
                            '',  # embl code
                            other.division_id,   # division id
                            '1',   # inherited div flag
                            '11',  # genetic code id
                            '1',   # inherited GC  flag
                            '0',   # mitochondrial genetic code id
                            '1',   # inherited MGC flag
                            '1',   # GenBank hidden flag
                            '0',   # hidden subtree root flag
                            '',  # comments
                        )))
                        f.write('\t|\n')

    def _write_bacdive_names(self):
        with self._oof('bacdive-names.dmp') as f:
            for taxon in self.bacdive_taxonomy.taxa(Taxon.has_rank(Rank.strain)):
                for al in self.mapping.get_21(taxon):
                    other = al.taxon1
                    if al.info['type'] == 'equivalent':
                        taxid = other.taxid
                        canonical_name = None
                        exclude_names = set(other.name_values())
                    else:
                        taxid = taxon.taxid
                        canonical_name = taxon.canonical_name
                        exclude_names = set()
                    for name in taxon.names():
                        if name.value not in exclude_names:
                            if name is canonical_name:
                                name_type = 'scientific name'
                            else:
                                name_type = name.name_type
                            f.write('\t|\t'.join((
                                taxid,
                                name.value,
                                '',
                                name_type
                            )))
                            f.write('\t|\n')

    def _write_ncbi_names(self):
        with self._oof('ncbi-names.dmp') as f:
            nsuppressed = 0
            for taxon in self.ncbi_taxonomy.taxa():
                names = list(taxon.names())
                nsuppressed += len(names)
                for al in self.mapping.get_12(taxon, lambda a: a.info['type'] == 'split'):
                    name_values = set(al.taxon2.name_values())
                    names = list(n for n in names if n.value not in name_values)
                    if taxon.canonical_name.value in name_values:
                        new = names[0]
                        Logger.warning('split removes canonical name \'%s\' from %s, new canonical is %s' % (taxon.canonical_name.value, taxon.taxid, new.value))
                        new.canonicalize()
                        new.name_type = 'scientific name'
                nsuppressed -= len(names)
                for name in names:
                    f.write('\t|\t'.join((
                        taxon.taxid,
                        name.value,
                        '',
                        name.name_type
                    )))
                    f.write('\t|\n')
            Logger.info('removed %d names from NCBI Taxonomy' % nsuppressed)


if __name__ == '__main__':
    BacdiveMatchApp().run()
