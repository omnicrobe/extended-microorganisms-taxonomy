configfile: 'config.yaml'


import datetime


OUTDIR=config['OUTDIR'] + '/' + config['MESH_DIR']
YEAR=str(datetime.date.today().year)


rule all:
    input:
        OUTDIR + '/d' + YEAR + '.bin'


rule mesh:
    output:
        OUTDIR + '/d' + YEAR + '.bin'

    params:
        year=YEAR

    shell:
        '''wget -O {output} 'https://nlmpubs.nlm.nih.gov/projects/mesh/MESH_FILES/asciimesh/d{params.year}.bin' '''


