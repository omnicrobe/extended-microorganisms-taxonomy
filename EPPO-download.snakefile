configfile: 'config.yaml'


rule all:
    input:
        fullcodes=config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/fullcodes.xml',
        xsd=config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/xmlfull.xsd',
        date=config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/DATE'

rule date:
    output:
        date=config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/DATE'

    shell:
         '''date '+%Y-%m-%d' >{output.date}'''


rule unzip:
    output:
        fullcodes=config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/fullcodes.xml',
        xsd=config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/xmlfull.xsd'

    input:
        config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/xmlfull.zip'

    shell:
        '''unzip -d {config[OUTDIR]}/{config[EPPO_DIR]} {input}'''


rule download:
    output:
        xmlfull=config['OUTDIR'] + '/' + config['EPPO_DIR'] + '/xmlfull.zip'

    shell:
        '''curl -o {output.xmlfull} '{config[EPPO_ZIP_URL]}' '''
