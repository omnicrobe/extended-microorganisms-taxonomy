import collections
import re


_prop_pattern = re.compile('([1A-Z_ ]+) = (.*)')


class Values(list):
    def __init__(self):
        super().__init__()

    def __bool__(self):
        return True


class Record(collections.defaultdict):
    def __init__(self):
        super().__init__(Values)

    def __getattr__(self, item):
        return self[item]

    @property
    def uid(self):
        return self.UI[0]


def records(path):
    current = Record()
    with open(path) as f:
        for line in f:
            line = line.strip()
            if line == '':
                continue
            if line == '*NEWRECORD':
                if 'UI' in current:
                    yield current
                current = Record()
                continue
            m = _prop_pattern.fullmatch(line)
            if m is None:
                raise RuntimeError('could not parse: ' + line)
            key = m.group(1)
            value = m.group(2)
            current[key].append(value)
    if 'UI' in current:
        yield current
