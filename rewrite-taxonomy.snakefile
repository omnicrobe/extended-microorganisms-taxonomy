configfile: 'config.yaml'


rule all:
    input:
        config['OUTDIR'] + '/taxid_microorganisms.txt',
        config['OUTDIR'] + '/taxa+id_microorganisms.txt',
        config['OUTDIR'] + '/taxid_full.txt',
        config['OUTDIR'] + '/taxa+id_full.txt',
        config['OUTDIR'] + '/taxid_header_microorganisms.txt',
        config['OUTDIR'] + '/taxa+id_header_microorganisms.txt',
        config['OUTDIR'] + '/taxid_header_full.txt',
        config['OUTDIR'] + '/taxa+id_header_full.txt'


rule microorganisms:
    output:
        config['OUTDIR'] + '/{p}_microorganisms.txt'

    input:
        full=config['OUTDIR'] + '/{p}_full.txt',
        roots='resources/microorganisms-roots.yaml',
        nodes=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/nodes.dmp'

    shell:
        '''scripts/cut-root.py {input.nodes} {input.roots} <{input.full} >{output}'''


rule taxaid_full:
    output:
        config['OUTDIR'] + '/taxa+id_full.txt'

    input:
        script='scripts/rewrite-taxonomy.py',
        bacdive_match=config['OUTDIR'] + '/' + config['BACDIVE_MATCH_DIR'],
        ncbi_dl=config['OUTDIR'] + '/' + config['NCBI_DIR'],
        saturators='resources/saturate.yaml',
        rejecters='resources/reject.yaml',
        pos_mapping='resources/POS-mapping.yaml',
        reject_name_types='resources/reject-name-types.txt'
    log:
        config['OUTDIR'] + '/taxa+id_full.log'

    params:
        wd=os.getcwd()

    shell:
        '''PYTHONPATH="{params.wd}" {input.script} --nodes {input.ncbi_dl}/nodes.dmp --nodes {input.bacdive_match}/bacdive-nodes.dmp --names {input.bacdive_match}/ncbi-names.dmp --names {input.bacdive_match}/bacdive-names.dmp --default-prefix ncbi --saturators {input.saturators} --rejecters {input.rejecters} --reject-name-types {input.reject_name_types} --iterate-names --output-pattern '{{NAME}}\t{{TAXID}}\t{{CANONICAL}}\t{{TAXID_PATH}}\t{{POS_TAG}}\t{{RANK}}\t{{SPECIES_TAXID}}\t{{SPECIES_NAME}}\n' --pos-mapping {input.pos_mapping} >{output} 2>{log}'''


rule taxid_full:
    output:
        config['OUTDIR'] + '/taxid_full.txt'

    log:
        config['OUTDIR'] + '/taxid_full.log'

    input:
        script='scripts/rewrite-taxonomy.py',
        bacdive_match=config['OUTDIR'] + '/' + config['BACDIVE_MATCH_DIR'],
        ncbi_dl=config['OUTDIR'] + '/' + config['NCBI_DIR'],
        saturators='resources/saturate.yaml',
        rejecters='resources/reject.yaml',
        pos_mapping='resources/POS-mapping.yaml',
        reject_name_types='resources/reject-name-types.txt'

    params:
        wd=os.getcwd()

    shell:
        '''PYTHONPATH="{params.wd}" {input.script} --nodes {input.ncbi_dl}/nodes.dmp --nodes {input.bacdive_match}/bacdive-nodes.dmp --names {input.bacdive_match}/ncbi-names.dmp --names {input.bacdive_match}/bacdive-names.dmp --default-prefix ncbi --saturators {input.saturators} --rejecters {input.rejecters} --reject-name-types {input.reject_name_types} --output-pattern '{{TAXID}}\t{{CANONICAL}}\t{{TAXID_PATH}}\t{{RANK}}\n' --pos-mapping {input.pos_mapping} >{output} 2>{log}'''


rule taxaid_header_full:
    output:
        config['OUTDIR'] + '/taxa+id_header_full.txt'

    input:
        script='scripts/rewrite-taxonomy.py',
        bacdive_match=config['OUTDIR'] + '/' + config['BACDIVE_MATCH_DIR'],
        ncbi_dl=config['OUTDIR'] + '/' + config['NCBI_DIR'],
        saturators='resources/saturate.yaml',
        rejecters='resources/reject.yaml',
        pos_mapping='resources/POS-mapping.yaml',
        reject_name_types='resources/reject-name-types.txt'

    log:
        config['OUTDIR'] + '/taxa+id_header_full.log'

    params:
        wd=os.getcwd()

    shell:
        '''PYTHONPATH="{params.wd}" {input.script} --nodes {input.ncbi_dl}/nodes.dmp --nodes {input.bacdive_match}/bacdive-nodes.dmp --names {input.bacdive_match}/ncbi-names.dmp --names {input.bacdive_match}/bacdive-names.dmp --default-prefix ncbi --saturators {input.saturators} --rejecters {input.rejecters} --reject-name-types {input.reject_name_types} --iterate-names --output-pattern '{{NAME}}\t{{TAXID}}\t{{CANONICAL}}\t{{TAXID_PATH}}\t{{POS_TAG}}\t{{RANK}}\t{{SPECIES_TAXID}}\t{{SPECIES_NAME}}\n' --output-header 'name\ttaxid\tcanonical-name\ttaxid-path\tpos\trank\tspecies-taxid\tspecies-name\n' --pos-mapping {input.pos_mapping} >{output} 2>{log}'''


rule taxid_header_full:
    output:
        config['OUTDIR'] + '/taxid_header_full.txt'

    log:
        config['OUTDIR'] + '/taxid_header_full.log'

    input:
        script='scripts/rewrite-taxonomy.py',
        bacdive_match=config['OUTDIR'] + '/' + config['BACDIVE_MATCH_DIR'],
        ncbi_dl=config['OUTDIR'] + '/' + config['NCBI_DIR'],
        saturators='resources/saturate.yaml',
        rejecters='resources/reject.yaml',
        pos_mapping='resources/POS-mapping.yaml',
        reject_name_types='resources/reject-name-types.txt'

    params:
        wd=os.getcwd()

    shell:
        '''PYTHONPATH="{params.wd}" {input.script} --nodes {input.ncbi_dl}/nodes.dmp --nodes {input.bacdive_match}/bacdive-nodes.dmp --names {input.bacdive_match}/ncbi-names.dmp --names {input.bacdive_match}/bacdive-names.dmp --default-prefix ncbi --saturators {input.saturators} --rejecters {input.rejecters} --reject-name-types {input.reject_name_types} --output-pattern '{{TAXID}}\t{{CANONICAL}}\t{{TAXID_PATH}}\t{{RANK}}\n' --output-header 'taxid\tcanonical-name\ttaxid-path\trank' --pos-mapping {input.pos_mapping} >{output} 2>{log}'''
