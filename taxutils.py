#!/bin/env python3

import logging
import re
import os.path
import collections
import json
import string
import yaml
import xml.etree.ElementTree as ET


logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
Logger = logging.getLogger(__name__)


class Sourced:
    def __init__(self, source, lineno):
        self._source = source
        self._lineno = lineno

    @property
    def source(self):
        return self._source

    @property
    def lineno(self):
        return self._lineno

    def message(self, msg=None):
        if msg:
            return '%s:%d: %s' % (self._source, self._lineno, msg)
        return '%s:%d' % (self._source, self._lineno)

    def info(self, msg):
        Logger.info(self.message(msg))

    def warning(self, msg):
        Logger.warning(self.message(msg))


class Name(Sourced):
    def __init__(self, source, lineno, taxon, value, name_type, lang='la', link=True):
        Sourced.__init__(self, source, lineno)
        self._taxon = taxon
        self._value = value
        self._name_type = name_type
        self._lang = lang
        self._linked = link
        if link:
            taxon._add_name(self)

    @property
    def taxon(self):
        return self._taxon

    @property
    def value(self):
        return self._value

    @property
    def name_type(self):
        return self._name_type

    @name_type.setter
    def name_type(self, name_type):
        self._name_type = name_type

    @property
    def lang(self):
        return self._lang

    @property
    def canonical(self):
        return self.taxon.canonical_name == self

    def link(self):
        if not self._linked:
            self._linked = True
            self._taxon._add_name(self)
        return self

    def canonicalize(self):
        self.taxon.canonical_name = self
        return self

    @staticmethod
    def has_name_type(name_type):
        return lambda name: name.name_type == name_type


def yes(_):
    return True


HIERACHICAL_RANK_NAMES = {
    'superkingdom': 1,
    'kingdom': 2,
    'subkingdom': 3,
    'superphylum': 4,
    'phylum': 5,
    'subphylum': 6,
    'infraphylum': 7,
    'superclass': 8,
    'class_': 9,
    'subclass': 10,
    'infraclass': 11,
    'cohort': 12,
    'subcohort': 13,
    'superorder': 14,
    'order': 15,
    'suborder': 16,
    'infraorder': 17,
    'parvorder': 18,
    'superfamily': 19,
    'family': 20,
    'subfamily': 21,
    'tribe': 22,
    'subtribe': 23,
    'genus': 24,
    'subgenus': 25,
    'section': 26,
    'subsection': 27,
    'series': 28,
    'subseries': 29,
    'speciesgroup': 30,
    'speciessubgroup': 31,
    'species': 32,
    'formaspecialis': 33,
    'subspecies': 34,
    'varietas': 35,
    'subvariety': 36,
    'forma': 37,
    'morph': 37,
    'pathogroup': 38,
    'serogroup': 38,
    'serotype': 39,
    'biotype': 39,
    'genotype': 39,
    'strain': 40,
    'isolate': 41
}


STANDARD_RANK_NAMES = {
    'kingdom',
    'phylum',
    'class_',
    'order',
    'family',
    'genus',
    'species'
}


class Rank:
    def __init__(self, name):
        attr = name.replace(' ', '').replace('-', '')
        if attr == 'class':
            attr = 'class_'
        if hasattr(Rank, attr):
            raise RuntimeError('duplicate rank ' + attr)
        self._name = name
        if attr in HIERACHICAL_RANK_NAMES:
            self._level = HIERACHICAL_RANK_NAMES[attr]
        else:
            self._level = None
        self._standard = (attr in STANDARD_RANK_NAMES)
        setattr(Rank, attr, self)

    @property
    def name(self):
        return self._name

    @property
    def level(self):
        return self._level

    @property
    def is_hierachical(self):
        return self._level is not None

    @property
    def is_standard(self):
        return self._standard


for rname in ('no rank', 'super-kingdom', 'kingdom', 'sub-kingdom', 'infra-kingdom', 'phylum', 'sub-phylum', 'infra-phylum', 'division', 'sub-division', 'super-class', 'clade', 'class', 'sub-class', 'infra-class', 'parv-class', 'legio', 'super-order', 'cohort', 'order', 'sub-order', 'infra-order', 'parv-order', 'super-family', 'family', 'sub-family', 'super-tribe', 'tribe', 'sub-tribe', 'genus', 'sub-genus', 'section', 'sub-section', 'series', 'sub-series', 'aggregate', 'species', 'semi-species', 'micro-species', 'sub-species', 'natio', 'variety', 'sub-variety', 'form', 'sub-form', 'forma-species', 'linea', 'clone', 'race', 'cultivar', 'morpha', 'abberatio', 'strain', 'serogroup', 'biotype', 'species group', 'isolate', 'serotype', 'species sub-group', 'sub-cohort', 'genotype', 'super-phylum', 'pathogroup'):
    Rank(rname)


class Taxon(Sourced):
    def __init__(self, source, lineno, taxid, rank=None):
        Sourced.__init__(self, source, lineno)
        self._taxid = taxid
        self._rank = rank
        self._parent = None
        self._children = []
        self._names = []
        self._canonical_name = None
        self._path = None
        self._ancestor = {}

    @property
    def taxid(self):
        return self._taxid

    @property
    def rank(self):
        return self._rank

    @property
    def parent(self):
        return self._parent

    @property
    def is_subspecific(self):
        if self.rank.is_hierachical:
            return self.rank.level > HIERACHICAL_RANK_NAMES['species']
        if self._parent is None:
            return False
        if self._parent._rank == Rank.species:
            return True
        return self._parent.is_subspecific

    @parent.setter
    def parent(self, parent):
        if self._parent is not None:
            raise RuntimeError('duplicate parent for %s (%s, then %s)' % (self._taxid, self._parent.taxid, parent.taxid))
        self._parent = parent
        parent._children.append(self)

    def move(self, parent):
        for desc in self.descendants(include_self=True):
            desc._path = None
            desc._ancestor.clear()
        self._parent._children.remove(self)
        self._parent = parent

    def children(self):
        yield from self._children

    def names(self, pred=yes):
        for n in self._names:
            if pred(n):
                yield n

    def has_name(self):
        return len(self._names) > 0

    def name_values(self, pred=yes):
        for n in self.names(pred):
            yield n.value

    def _add_name(self, name):
        self._names.append(name)

    @property
    def canonical_name(self):
        return self._canonical_name

    @canonical_name.setter
    def canonical_name(self, name):
        if name is not None and name.taxon != self:
            raise RuntimeError('cannot be canonical name of another taxon (taxon: %s, name: %s)' % (self, name))
        self._canonical_name = name

    @property
    def path(self):
        if self._path is None:
            if self._parent is None:
                self._path = '/' + self._taxid
            else:
                self._path = self._parent.path + '/' + self._taxid
        return self._path

    def ancestor(self, rank):
        if rank in self._ancestor:
            return self._ancestor[rank]
        for ancestor in self.ancestors():
            if ancestor.rank == rank:
                self._ancestor[rank] = ancestor
                return ancestor
        return None

    def ancestors(self, include_self=False, pred=yes):
        if include_self:
            ancestor = self
        else:
            ancestor = self._parent
        while ancestor is not None:
            if pred(ancestor):
                yield ancestor
            ancestor = ancestor.parent

    def descendants(self, include_self=False, pred=yes):
        if include_self and pred(self):
            yield self
        for child in self._children:
            yield from child.descendants(include_self=True, pred=pred)

    @staticmethod
    def has_rank(rank):
        return lambda taxon: taxon.rank == rank

    @staticmethod
    def has_not_rank(rank):
        return lambda taxon: taxon.rank != rank


class Taxonomy:
    def __init__(self):
        self._taxa = {}
        self._unresolved_parents = []

    def get_taxon(self, taxid):
        return self._taxa[taxid]

    def __getitem__(self, key):
        return self._taxa[key]

    def __contains__(self, key):
        return key in self._taxa

    def __len__(self):
        return len(self._taxa)

    def delete_taxon(self, taxon):
        if self._unresolved_parents:
            raise RuntimeError('there are unresolved parents')
        for desc in taxon.descendants(include_self=True):
            del self._taxa[desc.taxid]

    def taxa(self, pred=yes):
        if self._unresolved_parents:
            raise RuntimeError('there are unresolved parents')
        for t in self._taxa.values():
            if pred(t):
                yield t

    def root_taxa(self):
        if self._unresolved_parents:
            raise RuntimeError('there are unresolved parents')
        for taxon in self._taxa.values():
            if taxon.parent is None:
                yield taxon

    def top_down_taxa(self, pred=yes):
        for root in self.root_taxa():
            yield from root.descendants(include_self=True, pred=pred)

    def add_unresolved_parent(self, child, parent):
        self._unresolved_parents.append((child, parent))

    def add_taxon(self, source, lineno, taxid, rank):
        if taxid in self._taxa:
            raise ValueError('duplicate taxid %s' % taxid)
        taxon = Taxon(source, lineno, taxid, rank)
        self._taxa[taxid] = taxon
        return taxon

    def resolve_parents(self):
        Logger.info('resolving taxa parents')
        for taxid, parent_taxid in self._unresolved_parents:
            taxon = self[taxid]
            taxon.parent = self[parent_taxid]
        self._unresolved_parents = []

    def get_all_names(self):
        for taxon in self._taxa.values():
            for name in taxon.names():
                yield name

    def get_name_map(self):
        name_map = {}
        for name in self.get_all_names():
            if name.value in name_map:
                name_map[name.value].append(name)
            else:
                name_map[name.value] = [name]
        return name_map


class BacDiveParser:
    LPSN_ID_PREFIX = 'lpsn:'
    BACDIVE_ID_PREFIX = 'bd:'
    OLD_COLLECTIONS_SATURATOR = [
        {
            'name_type': 'old collection HKI',
            'name_type_retriction': 'designation',
            'pattern': r'ST\s*(.*)\s*\(HKI\)',
            'replacements': ['ST{1}', 'ST {1}']
        },
        {
            'name_type': 'old collection IMET',
            'name_type_retriction': 'designation',
            'pattern': r'STI\s*(.*)\s*\(IMET\)',
            'replacements': ['STI{1}', 'STI {1}']
        },
        {
            'name_type': 'old collection ZIMET',
            'name_type_retriction': 'designation',
            'pattern': r'STH\s*(.*)\s*\(ZIMET\)',
            'replacements': ['STH{1}', 'STH {1}']
        },
        {
            'name_type': 'old collection SFU',
            'name_type_retriction': 'designation',
            'pattern': r'SF\s*(.*)\s*\(FSU\)',
            'replacements': ['SF{1}', 'SF {1}']
        }
    ]
    # OLD_COLLECTIONS_SATURATOR = '''designation	ST\s*(.*)\s*\(HKI\)	old collection HKI	ST{1}	ST {1}
# designation	STI\s*(.*)\s*\(IMET\)	old collection IMET	STI{1}	STI {1}
# designation	STH\s*(.*)\s*\(ZIMET\)	old collection ZIMET	STH{1}	STH {1}
# designation	SF\s*(.*)\s*\(FSU\)	old collection FSU	SF{1}	SF {1}
# '''

    @staticmethod
    def read_directory(path, taxonomy=None):
        old_collections_saturator = Saturator._from_yaml_object(BacDiveParser.OLD_COLLECTIONS_SATURATOR)
        Logger.info('reading BacDive entries in directory: %s' % path)
        if taxonomy is None:
            taxonomy = Taxonomy()
        for dirpath, _, filenames in os.walk(path):
            for fn in filenames:
                if not fn.endswith('.json'):
                    continue
                BacDiveParser.read_json_file(taxonomy, os.path.join(dirpath, fn), old_collections_saturator)
        return taxonomy

    @staticmethod
    def read_json_file(taxonomy, filename, old_collections_saturator):
        with open(filename) as f:
            j = json.load(f)
            res = j['results']
            if res:
                for entry in res.values():
                    BacDiveParser.read_entry(taxonomy, filename, entry, old_collections_saturator)

    @staticmethod
    def read_entry(taxonomy, filename, entry, old_collections_saturator):
        bacdive_id = BacDiveParser.BACDIVE_ID_PREFIX + str(entry['General']['BacDive-ID'])
        taxon = taxonomy.add_taxon(filename, 0, bacdive_id, Rank.strain)
        parent = BacDiveParser._read_lineage(taxonomy, filename, entry)
        if parent is None:
            Logger.warning('no lineage for %s' % bacdive_id)
        else:
            taxon.parent = parent
        for des in BacDiveParser._read_designations(entry):
            Name(filename, 0, taxon, des, 'designation')
        for name in taxon.names():
            for cn in old_collections_saturator.saturate(name):
                cn.link()
        for sn in BacDiveParser._read_strain_numbers(entry):
            Name(filename, 0, taxon, sn, 'collection number')
        if not taxon.has_name():
            Logger.warning('entry %s has no name, dropping' % taxon.taxid)
            taxonomy.delete_taxon(taxon)
            return
        for name in taxon.names():
            for cn in BacdiveStrainCompleteNameSaturator().saturate(name):
                cn.link()
        BacDiveParser._find_canonical_name(taxon)

    @staticmethod
    def _find_canonical_name(taxon):
        for name in taxon.names(pred=lambda n: n.name_type == 'complete_name' and n.value.contains('DSM ')):
            name.canonicalize()
        if taxon.canonical_name is None:
            for name in taxon.names(pred=Name.has_name_type('complete name')):
                name.canonicalize()
                break
        if taxon.canonical_name is None:
            for name in taxon.names():
                name.canonicalize()
                break

    @staticmethod
    def _read_strain_numbers(entry):
        ext = entry['External links']
        if 'culture collection no.' in ext:
            ccn = ext['culture collection no.']
            for n in re.split(', ', ccn):
                yield ' '.join(n.strip().split())

    @staticmethod
    def _read_designations(entry):
        natc = entry['Name and taxonomic classification']
        if 'strain designation' in natc:
            desig = natc['strain designation']
            for n in re.split(', ', desig):
                yield ' '.join(n.strip().split())

    @staticmethod
    def _read_lineage(taxonomy, filename, entry):
        lineage = entry['Name and taxonomic classification']
        if 'LPSN' in lineage:
            lineage = lineage['LPSN']
        taxon, is_new = BacDiveParser._read_taxon(taxonomy, filename, lineage, 'domain', Rank.superkingdom, None, False)
        taxon, is_new = BacDiveParser._read_taxon(taxonomy, filename, lineage, 'phylum', Rank.phylum, taxon, is_new)
        taxon, is_new = BacDiveParser._read_taxon(taxonomy, filename, lineage, 'class', Rank.class_, taxon, is_new)
        taxon, is_new = BacDiveParser._read_taxon(taxonomy, filename, lineage, 'order', Rank.order, taxon, is_new)
        taxon, is_new = BacDiveParser._read_taxon(taxonomy, filename, lineage, 'family', Rank.family, taxon, is_new)
        taxon, is_new = BacDiveParser._read_taxon(taxonomy, filename, lineage, 'genus', Rank.genus, taxon, is_new)
        taxon, is_new = BacDiveParser._read_taxon(taxonomy, filename, lineage, 'species', Rank.species, taxon, is_new)
        if is_new and 'synonyms' in lineage:
            syns = lineage['synonyms']
            if isinstance(syns, dict):
                syns = [syns]
            for syn in syns:
                Name(filename, 0, taxon, syn['synonym'], 'synonym')
        return taxon

    @staticmethod
    def _read_taxon(taxonomy, filename, lineage, key, rank, parent, parent_is_new):
        if key not in lineage:
            return parent, parent_is_new
        name = lineage[key]
        if 'unclassified' in name or 'Not assigned' in name or 'not assigned' in name or 'not further classified' in name:
            return parent, parent_is_new
        if rank == Rank.species and name.endswith(' sp.'):
            return parent, parent_is_new
        taxid = BacDiveParser.LPSN_ID_PREFIX + name
        if taxid in taxonomy:
            return taxonomy[taxid], False
        taxon = taxonomy.add_taxon(filename, 0, taxid, rank)
        if parent is not None:
            taxon.parent = parent
        Name(filename, 0, taxon, name, 'scientific name').canonicalize()
        return taxon, True


class EPPOParser:
    RANK_MAP = {
        'K': Rank.kingdom,
        'A': Rank.clade,
        'P': Rank.phylum,
        'Q': Rank.subphylum,
        'C': Rank.class_,
        'D': Rank.norank,
        'L': Rank.subclass,
        'O': Rank.order,
        'R': Rank.suborder,
        'F': Rank.family,
        'S': Rank.subfamily,
        'G': Rank.genus
    }

    @staticmethod
    def read_xmlfull_file(filename, taxonomy=None):
        if taxonomy is None:
            taxonomy = Taxonomy()
        Logger.info('reading EPPO codes: %s' % filename)
        tree = ET.parse(filename)
        root = tree.getroot()
        for xcode in root.findall('./code[@isactive = "true"]'):
            EPPOParser._read_xmlfull_code(filename, taxonomy, xcode)
        return taxonomy

    @staticmethod
    def _read_xmlfull_code(source, taxonomy, xcode):
        type_ = xcode.attrib['type']
        if type_ == 'NTX':
            return
        id_ = int(xcode.attrib['id'])
        eppocode = xcode.findtext('./eppocode')
        rank = EPPOParser._parse_rank(eppocode)
        taxid = EPPOParser._build_taxid(eppocode)
        taxon = taxonomy.add_taxon(source, id_, taxid, rank)
        for xname in xcode.findall('./names/name[@isactive = "true"]'):
            EPPOParser._read_xmlfull_name(source, taxon, xname)
        for xparent in xcode.findall('./parents/parent[@linktype = "taxo"]'):
            taxonomy.add_unresolved_parent(taxid, EPPOParser._build_taxid(xparent.text))

    @staticmethod
    def _parse_rank(eppocode):
        if eppocode.startswith('1'):
            rank_code = eppocode[-1]
            if rank_code not in EPPOParser.RANK_MAP:
                raise RuntimeError('could not determine rank for %s' % eppocode)
            return EPPOParser.RANK_MAP[rank_code]
        return Rank.species

    @staticmethod
    def _build_taxid(eppocode):
        return 'eppo:' + eppocode

    @staticmethod
    def _read_xmlfull_name(source, taxon, xname):
        nid = int(xname.attrib['id'])
        fullname = xname.findtext('./fullname').replace('\t', ' ')
        lang = xname.findtext('./lang')
        authority = xname.findtext('./authority')
        if lang == 'la':
            name_type = 'scientific'
        else:
            name_type = 'vernacular'
        name = Name(source, nid, taxon, fullname, name_type, lang=lang)
        if xname.attrib['ispreferred'] == 'true':
            if authority is not None:
                authname = Name(source, nid, taxon, ' '.join([fullname, authority]), 'scientific with authority', lang=lang)
                authname.canonicalize()
            else:
                name.canonicalize()


class NCBIParser:
    CANONICAL_NAME_TYPE = 'scientific name'
    ID_PREFIX = 'ncbi:'

    @staticmethod
    def _get_rank(name):
        attr = name.replace(' ', '')
        if attr == '':  # as of 2024-04-12 the NCBI taxonomy nodes.dmp has empty rank for subclasses
            return Rank.subclass  # TODO: remove fix when NCBI has fixed nodes.dmp
        if attr == 'class':
            return Rank.class_
        if attr == 'forma' or attr == 'formaspecialis':
            return Rank.formaspecies
        if attr == 'varietas':
            return Rank.variety
        if attr == 'morph':
            return Rank.morpha
        return getattr(Rank, attr)

    @staticmethod
    def _build_taxid(taxid):
        if ':' in taxid:
            return taxid
        return NCBIParser.ID_PREFIX + taxid

    @staticmethod
    def read_node_file(f, taxonomy=None):
        if taxonomy is None:
            taxonomy = Taxonomy()
        Logger.info('reading NCBI nodes: %s' % f.name)
        for lineno, line in enumerate(f, start=1):
            NCBIParser._read_node_line(taxonomy, f.name, lineno, line)
        return taxonomy

    @staticmethod
    def _read_node_line(taxonomy, source, lineno, line):
        cols = line.split('|', 4)
        taxid = NCBIParser._build_taxid(cols[0].strip())
        rank = NCBIParser._get_rank(cols[2].strip())
        parent_taxid = NCBIParser._build_taxid(cols[1].strip())
        taxon = taxonomy.add_taxon(source, lineno, taxid, rank)
        taxon.division_id = cols[4].strip()
        if parent_taxid != taxid:
            taxonomy.add_unresolved_parent(taxid, parent_taxid)

    @staticmethod
    def read_name_file(f, taxonomy, reject_name_types=None, log_reject=True):
        if reject_name_types is None:
            reject_name_types = set()
        Logger.info('reading NCBI names: %s' % f.name)
        for lineno, line in enumerate(f, start=1):
            NCBIParser._read_name_line(taxonomy, f.name, lineno, reject_name_types, line, log_reject)

    @staticmethod
    def _read_name_line(taxonomy, source, lineno, reject_name_types, line, log_reject=True):
        cols = line.split('|', 4)
        name_type = cols[3].strip()
        value = cols[1].strip()
        taxid = NCBIParser._build_taxid(cols[0].strip())
        if name_type not in reject_name_types:
            taxon = taxonomy[taxid]
            name = Name(source, lineno, taxon, value, name_type)
            if name_type == NCBIParser.CANONICAL_NAME_TYPE:
                name.canonicalize()
        elif log_reject:
            Sourced(source, lineno).info('ignore name \'%s\' of type \'%s\'' % (value, name_type))


class Saturator:
    def __init__(self):
        pass

    def saturate(self, name):
        yield from []

    def saturate_plus_original(self, name):
        yield name
        yield from self.saturate(name)

    def saturate_taxon_names(self, taxon):
        unique = {}
        for name in taxon.names():
            for sat in self.saturate_plus_original(name):
                unique[sat.value] = sat
        return unique.values()

    def saturate_reject(self, rejecter, taxon):
        for name in self.saturate_taxon_names(taxon):
            if rejecter.accept(name):
                yield name

    @staticmethod
    def _from_yaml_object(obj):
        if isinstance(obj, list):
            return SaturatorCollection(Saturator._from_yaml_object(elt) for elt in obj)
        if isinstance(obj, dict):
            return PatternSaturator(obj['pattern'], obj['name_type'], obj['replacements'], obj.get('name_type_restriction', None))
        raise RuntimeError('cannot convert: ' + str(obj))

    @staticmethod
    def from_yaml_file(filename):
        with open(filename) as f:
            y = yaml.load(f, Loader=yaml.FullLoader)
            return Saturator._from_yaml_object(y)


class BacdiveStrainCompleteNameSaturator(Saturator):
    def __init__(self):
        Saturator.__init__(self)

    def saturate(self, name):
        if (name.name_type == 'collection number' or name.name_type == 'designation') and name.taxon.parent is not None:
            for parent_name in name.taxon.parent.names():
                if parent_name.canonical:
                    name_type = 'complete name'
                else:
                    name_type = 'complete synonym'
                value = parent_name.value + ' ' + name.value
                yield Name(name.source, name.lineno, name.taxon, value, name_type, link=False)


class SaturatorCollection(Saturator):
    def __init__(self, saturators):
        Saturator.__init__(self)
        self._saturators = list(saturators)

    def saturate(self, name):
        for saturator in self._saturators:
            yield from saturator.saturate(name)


class ExtendedFormatter(string.Formatter):
    def convert_field(self, value, conversion):
        if conversion == "u":
            return str(value).upper()
        elif conversion == "l":
            return str(value).lower()
        return super(ExtendedFormatter, self).convert_field(value, conversion)


class PatternSaturator(Saturator):
    FORMATTER = ExtendedFormatter()

    def __init__(self, pattern, name_type, replacements, name_type_restriction=None):
        Saturator.__init__(self)
        self.pattern = re.compile(pattern)
        self.name_type = name_type
        self.replacements = tuple(replacements)
        if name_type_restriction is None:
            self.name_type_restriction = None
        else:
            self.name_type_restriction = set(name_type_restriction)

    def saturate(self, name):
        if self.name_type_restriction is None or name.name_type in self.name_type_restriction:
            match = self.pattern.fullmatch(name.value)
            if match is not None:
                groups = list(match.groups())
                groups.insert(0, match.groups(0))
                for rep in self.replacements:
                    value = PatternSaturator.FORMATTER.format(rep, *groups)
                    yield Name(name.source, name.lineno, name.taxon, value, self.name_type, link=False)


class Rejecter:
    def __init__(self):
        pass

    def reject(self, name):
        return False

    def accept(self, name):
        return not self.reject(name)

    @staticmethod
    def _from_yaml_object(obj):
        if isinstance(obj, list):
            return RejecterCollection(Rejecter._from_yaml_object(elt) for elt in obj)
        if isinstance(obj, dict):
            if 'pattern' in obj:
                pattern = obj['pattern']
                rejecter = PatternRejecter(pattern)
                if 'taxid' in obj:
                    return TaxidRestrictedRejecter(obj['taxid'], rejecter)
                return rejecter
            if 'taxid' in obj:
                return TaxidRejecter(obj['taxid'])
        raise RuntimeError('cannot convert: ' + str(obj))

    @staticmethod
    def from_yaml_file(filename):
        with open(filename) as f:
            y = yaml.load(f, Loader=yaml.FullLoader)
            return Rejecter._from_yaml_object(y)


class TaxidRestrictedRejecter(Rejecter):
    def __init__(self, taxid, rejecter):
        Rejecter.__init__(self)
        self._taxid = taxid
        self._rejecter = rejecter

    def reject(self, name):
        if name.taxon.taxid == self._taxid:
            return self._rejecter.reject(name)
        return False


class TaxidRejecter(Rejecter):
    def __init__(self, taxid):
        Rejecter.__init__(self)
        self._taxid = taxid

    def reject(self, name):
        return name.taxon.taxid == self._taxid


class RejecterCollection(Rejecter):
    def __init__(self, rejecters):
        Rejecter.__init__(self)
        self._rejecters = list(rejecters)

    def reject(self, name):
        for rej in self._rejecters:
            if rej.reject(name):
                return True
        return False


class PatternRejecter:
    def __init__(self, pattern):
        self._pattern = re.compile(pattern)

    def reject(self, name):
        match = self._pattern.fullmatch(name.value)
        result = match is not None
        if result:
            Logger.info('reject name \'%s\' (%s) because pattern \'%s\'' % (name.value, name.taxon.taxid, self._pattern.pattern))
        return result


class Alignment:
    def __init__(self, taxon1, taxon2):
        self._taxon1 = taxon1
        self._taxon2 = taxon2
        self.info = {}

    @property
    def taxon1(self):
        return self._taxon1

    @property
    def taxon2(self):
        return self._taxon2


class TaxonomyMapping:
    def __init__(self, taxonomy1, taxonomy2):
        self._taxonomy1 = taxonomy1
        self._taxonomy2 = taxonomy2
        self._map_12 = collections.defaultdict(dict)
        self._map_21 = collections.defaultdict(dict)

    @property
    def taxonomy1(self):
        return self._taxonomy1

    @property
    def taxonomy2(self):
        return self._taxonomy2

    def _extend_alignment(self, taxon1, taxon2, create=False, block=False):
        d2 = self._map_12[taxon1.taxid]
        if taxon2.taxid in d2:
            if block:
                raise RuntimeError('already exists')
            return d2[taxon2.taxid]
        if create:
            al = Alignment(taxon1, taxon2)
            d2[taxon2.taxid] = al
            self._map_21[taxon2.taxid][taxon1.taxid] = al
            return al
        raise RuntimeError('does not exist')

    def add_alignment(self, taxon1, taxon2):
        return self._extend_alignment(taxon1, taxon2, create=True, block=True)

    def get_alignment(self, taxon1, taxon2):
        return self._extend_alignment(taxon1, taxon2, create=False, block=False)

    def extend_alignment(self, taxon1, taxon2):
        return self._extend_alignment(taxon1, taxon2, create=True, block=False)

    def has_alignment(self, taxon1, taxon2):
        return taxon1.taxid in self._map_12 and taxon2.taxid in self._map_12[taxon1.taxid]

    def remove_alignment(self, taxon1, taxon2):
        del self._map_12[taxon1.taxid][taxon2.taxid]
        del self._map_21[taxon2.taxid][taxon1.taxid]

    def get_12(self, taxon1, pred=yes):
        if taxon1.taxid in self._map_12:
            for al in self._map_12[taxon1.taxid].values():
                if pred(al):
                    yield al

    def get_21(self, taxon2, pred=yes):
        if taxon2.taxid in self._map_21:
            for al in self._map_21[taxon2.taxid].values():
                if pred(al):
                    yield al

    def has_12(self, taxon1, pred=yes):
        for _ in self.get_12(taxon1, pred):
            return True
        return False

    def has_21(self, taxon2, pred=yes):
        if taxon2.taxid in self._map_21:
            for a in self._map_21[taxon2.taxid]:
                if pred(a):
                    return True
        return False

    def get_alignments(self):
        for d2 in self._map_12.values():
            yield from d2.values()
