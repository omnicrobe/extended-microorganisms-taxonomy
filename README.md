# Extended microorganisms taxonomy

*Extended microorganisms taxonomy* is a set of tools for selecting microorganisms and merging taxonomies and nomenclatures from different sources.

At the moment, the *Extended microorganisms taxonomy* selects microorganisms subtrees from the [NCBI Taxonomy](https://www.ncbi.nlm.nih.gov/taxonomy) and appends/merges entries from [BacDive](https://bacdive.dsmz.de/).

## Workflow

### 0. Configuration

Edit the `config.yaml` file, and set the following variables:

* `BACDIVE_USER`: username for the BacDive API
* `BACDIVE_PASSWORD_FILE`: file containing the password for the BacDive API
* `ALVISNLP`: path to the AlvisNLP binary


### 1. NCBI Taxonomy download

```shell
snakemake -j 1 -s ncbi-download.snakefile --printshellcmds
```

**ETA: 2 minutes**

This will download the Taxonomy archive from the NCBI FTP server, then unzip the archive.

The current date is stored in a file named `DATE`.

The download is anonymous and does not require any registration.

### 2. BacDive entries download

```shell
snakemake -j 1 -s bacdive-download.snakefile --printshellcmds
```

**ETA: 20 minutes or 3 hours**

This will download the whole DSMZ catalog via the BacDive service.

In order to use the BacDive API, you must [register](https://api.bacdive.dsmz.de/).
You must type the registered user to the variable `BACDIVE_API_USER` in the `config.yaml` file.
You also must record the password in a file, then type the file path to the variable `BACDIVE_API_PASSWORD_FILE`.
**Do not forget to `chmod 600` this file.

### 3. Match BacDive to NCBI taxonomy

```shell
snakemake -j 1 -s bacdive-match.snakefile --printshellcmds
```

**ETA: 2 minutes**

This will look for a suitable place in the NCBI Taxonomy for each DMSZ strain.

The output of this step contains 5 files:
* `bacdive-to-taxid.txt`: a mapping file from the BacDive identifier of the strain to the taxon identifier
* `dispatch-report.txt`: a report of the dispatch for each BacDive entry
* `bacdive-nodes.dmp` and `bacdive-names.dmp`: files in the format of the NCBI Taxonomy with additional nodes and synonyms
* `warnings.txt`: things to pay attention


### 4. Rewrite taxonomy

```shell
snakemake -j 1 -s rewrite-taxonomy.snakefile --printshellcmds
```

**ETA: 75 minutes**

This will write the merged taxonomy in a format suitable for text projection.

## Resulting taxonomy

### Namespace prefixes

| Prefix | Domain | URL construct |
|--------|--------|---------------|
| `ncbi` | NCBI Taxonomy | `https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id={ID}` (web page) |
| | | `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id={ID}` (API) |
| `bd`   | BacDive | `https://bacdive.dsmz.de/strain/{ID}` (web page) |
| | | `https://api.bacdive.dsmz.de/example/fetch/{ID}` (API, authentication required) |


### 5. Compile taxonomy tries


```shell
snakemake -j 1 -s compile-trie.snakefile --printshellcmds
```

**ETA: 10 minutes**

This will compile taxonomy tries for future use with AlvisNLP.

The output of this step contains 2 files:
* `taxa+id_full.trie`
* `taxa+id_microorganisms.trie`

## Generation of the Extended taxonomy

The extended taxonomy is automatically and regularly generated. It can be downloaded by following this link https://omnicrobe.pages.mia.inra.fr/extended-microorganisms-taxonomy 
