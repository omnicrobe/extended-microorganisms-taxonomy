configfile: 'config.yaml'


rule all:
    input:
        nodes=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/nodes.dmp',
        names=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/names.dmp',
        date=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/DATE'

rule date:
    output:
        date=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/DATE'

    shell:
         '''date '+%Y-%m-%d' >{output.date}'''


rule unzip:
    output:
        nodes=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/nodes.dmp',
        names=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/names.dmp'

    input:
        config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/taxdmp.zip'

    shell:
        '''unzip -d {config[OUTDIR]}/{config[NCBI_DIR]} {input}'''


rule download:
    output:
        taxdmp=config['OUTDIR'] + '/' + config['NCBI_DIR'] + '/taxdmp.zip'

    shell:
        '''curl -o {output.taxdmp} '{config[NCBI_ZIP_URL]}' '''
