configfile: 'config.yaml'


rule match:
    '''
    Match strain names to NCBI nodes.
    This step uses `alvisnlp`.
    '''
    output:
        directory(config['OUTDIR'] + '/' + config['BACDIVE_MATCH_DIR'])

    input:
        script='scripts/bacdive-match.py',
        strains=config['OUTDIR'] + '/' + config['BACDIVE_STRAINS_DIR'],
        ncbi_dl=config['OUTDIR'] + '/' + config['NCBI_DIR'],
        disqualifying_adj='resources/mapping-disqualifying-adjectives.txt',
        legit_coll='resources/legitimate-collections.txt',
        priority_coll='resources/priority-collections.txt'

    params:
        wd=os.getcwd()

    shell: '''mkdir -p {output} && PYTHONPATH="{params.wd}" {input.script} --nodes {input.ncbi_dl}/nodes.dmp --names {input.ncbi_dl}/names.dmp --bacdive {input.strains} --disqualifying-adjectives {input.disqualifying_adj} --legitimate-collections {input.legit_coll} --priority-collections {input.priority_coll} --output {output} 2>{output}/bacdive-match.log'''
