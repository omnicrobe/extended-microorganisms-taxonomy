configfile: 'config.yaml'


rule all:
    input:
        config['OUTDIR'] + '/taxa+id_microorganisms.trie',
        config['OUTDIR'] + '/taxa+id_full.trie'


rule compile:
    input:
        taxo=config['OUTDIR'] + '/taxa+id_{root}.txt',
        plan='alvisnlp/compile-taxonomy.plan'

    output:
        config['OUTDIR'] + '/taxa+id_{root}.trie'

    singularity:config["ALVISNLP_SINGULARITY_IMG"]

    shell:
        '''{config[ALVISNLP]} -J-Xmx24G -alias taxo {input.taxo} -alias trie {output} {input.plan}'''
